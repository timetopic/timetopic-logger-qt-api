/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

var ws;
var newline = String.fromCharCode(10);

var contextName = "default";

function TTPLOG_SET_CONTEXT_NAME(newName) {
	contextName = newName;
}

function TTPLOG_EVENT_START(channel) {
	//http://timetopic.herwoodtechnologies.com/resources/file-format
	//http://timetopic.herwoodtechnologies.com/resources/log-item-syntax
	//timestamp;event;start;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";event;start;["+contextName+"]"+channel;
	logText( ret ) ;
}

function TTPLOG_EVENT_STOP(channel) {
	//timestamp;event;stop;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";event;stop;["+contextName+"]"+channel;
	logText( ret ) ;
}

function TTPLOG_NAMED_EVENT_START(channel, comment) {
	//http://timetopic.herwoodtechnologies.com/resources/file-format
	//http://timetopic.herwoodtechnologies.com/resources/log-item-syntax
	//timestamp;event;start;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";namedevent;start;["+contextName+"]"+channel+";"+comment;
	logText( ret ) ;
}

function TTPLOG_NAMED_EVENT_STOP(channel) {
	//timestamp;event;stop;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";namedevent;stop;["+contextName+"]"+channel;
	logText( ret ) ;
}

function TTPLOG_VALUE(channel, value){
	//timestamp;valueabs;value;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";valueabs;"+value+";["+contextName+"]"+channel;
	logText( ret ) ;
}

function TTPLOG_STATE(channel, state){
	//timestamp;state;stateMessage;channelname[CR]LF
	var ret;
	ret = GetTimeStamp()+";state;"+state+";["+contextName+"]"+channel;
	logText( ret ) ;
}
function TTPLOG_STRING(str){

	var ret = GetTimeStamp()+";text;"+str

	logText( ret ) ;
}

function pad(n){
	//http://stackoverflow.com/questions/3605214/javascript-add-leading-zeroes-to-date
	return n<10 ? '0'+n : n
}

function GetTimeStamp() {			
	var d = new Date();
	//yyyy/MM/dd hh:mm:ss.zzz		
	var	timestamp = d.getFullYear()+"/"+pad(1+d.getMonth())+"/"+pad(d.getDate())+" "+pad(d.getHours())+":"+pad(d.getMinutes())+":"+pad(d.getSeconds())+"."+d.getMilliseconds();					
	//var	timestamp = d.toISOString(); // currently not supported by timetopic	
	return timestamp
}

function logText(myTxt) {
	txToWs (myTxt)
}

function txToWs(msg) {
	ws.send(msg+newline);
}

ws.onopen = function () {
	TTPLOG_EVENT_START("ws.onopen")	
	log('Connected to Logger');		
	TTPLOG_EVENT_STOP("ws.onopen")
};

function delay(amount) {
	for (k=0;k<amount;k++) {
		 log('delay');
	}
}

function TTPLOG_INIT(url) {

ws = new WebSocket(url);		

ws.onmessage = function (evt) {
	var msg = evt.data;
	log('message received' + msg);
};

ws.onclose = function () {
	log('connection closed');
};

ws.onerror = function (e) {
	log('connection error');
};

}

function log(txt) {

	// implement here possible error handler if needed. 
	// var div = document.getElementById('log');
	// div.innerHTML = div.innerHTML + txt + '<br>';

}