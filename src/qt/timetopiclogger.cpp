/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QDateTime>
#include <QFile>
#include <QProcess>
#include <QStringList>
#include <QTextStream>
#include <stdio.h>
#include <stdlib.h>
//
#include "timetopiclogger.h"

void timeToPicLoggerDoDeleteLater(QObject *obj) { obj->deleteLater(); }

TimeToPicLogger *TimeToPicLogger::m_instance = nullptr;

void myMessageOutput(QtMsgType /*type*/,
                     const QMessageLogContext & /*context */,
                     const QString &msg) {

  /* Forward data to log transport */
  TimeToPicLogger::instance()->logString(msg);
}

TimeToPicLogger::TimeToPicLogger() {
  m_prevProcessTime = 0;
  m_prevTotalTime = 0;
  m_elapsed.start();
}

TimeToPicLogger *TimeToPicLogger::instance() {
  static QMutex instanceMutex;
  QMutexLocker locker(&instanceMutex);
  Q_UNUSED(locker);

  if (m_instance == nullptr) {
    m_instance = new TimeToPicLogger();
    m_instance->start();
  }
  return m_instance;
}

void TimeToPicLogger::run() {
#ifdef SERVERMODE
  LogStreamThread thread(LogStreamThread::modeServer);
#else
  LogStreamThread thread(LogStreamThread::modeClient);
#endif
  connect(this, &TimeToPicLogger::newLogRow, &thread,
          &LogStreamThread::writeRow);
  connect(&m_performanceCounterTimer, &QTimer::timeout, this,
          &TimeToPicLogger::performanceCounterTimerSlot);
  exec();
}

void TimeToPicLogger::stop() {
  /* Stop possible performance timer */
  m_performanceCounterTimer.stop();

  exit();
  const int exitTimeTimeOutMs =
      500; // wait for maximum of 500ms for run function to exit
  wait(exitTimeTimeOutMs);
}

void TimeToPicLogger::logString(const QString &text) {
  emit newLogRow(getTimestamp() + ";" + text);
}

void TimeToPicLogger::logEvent(const QString &channel, EventState eventState) {
  // mutex is here just to make sure that items are in timely order
  QString stateString;
  switch (eventState) {
  case EventStart:
    stateString = "start";
    break;
  case EventStop:
    stateString = "stop";
    break;
  }
  QString concat = "event;" + stateString + ";" + channel;
  qDebug().noquote() << concat;
}

void TimeToPicLogger::logEvent(const QString &channel, bool startEvent) {
  if (startEvent) {
    logEvent(channel, EventStart);
  } else {
    logEvent(channel, EventStop);
  }
}

void TimeToPicLogger::logMessage(const QString &source,
                                 const QString &destination,
                                 const QString &messageName) {
  QString concat = "msg;" + source + ";" + destination + ";" + messageName;
  qDebug().noquote() << concat;
}

void TimeToPicLogger::logState(const QString &channel, const QString &state) {
  QMutexLocker locker(&m_mutex);
  Q_UNUSED(locker);
  if (m_statemachineCurrentState[channel] != state) {
    m_statemachineCurrentState[channel] = state;

    QString concat = "state;" + state + ";" + channel;
    qDebug().noquote() << concat;
  }
}

void TimeToPicLogger::logValue(const QString &channel, const QString &value) {
  QString concat = "valueabs;" + value + ";" + channel;
  qDebug().noquote() << concat;
}

TimeToPicLoggerUserPerformanceCounter *
TimeToPicLogger::GetCounter(const QString &counterName) {
  QMutexLocker locker(&m_userCountersMutex);
  if (mUserCounters.contains(counterName) == false) {
    mUserCounters[counterName] = new TimeToPicLoggerUserPerformanceCounter();
    mUserCounters[counterName]->setCounterName(counterName);
  }
  return mUserCounters[counterName];
}

void TimeToPicLogger::userPerformanceCounterSet(const QString &counterName,
                                                const quint32 &counterValue) {

  GetCounter(counterName)->setCounterValue(counterValue);
}

void TimeToPicLogger::userPerformanceCounterIncrement(
    const QString &counterName, const quint32 &increment) {
  GetCounter(counterName)->incrementCounter(increment);
}

void TimeToPicLogger::performanceCounterTimerSlot() { reportUserCounters(); }

qreal TimeToPicLogger::getTimeStampOffsetMsec() const
{
    return m_timeStampOffsetMsec;
}

void TimeToPicLogger::setTimeStampOffsetMsec(const qreal &timeStampOffsetMsec)
{
    m_timeStampOffsetMsec = timeStampOffsetMsec;
}

void TimeToPicLogger::reportUserCounters() {
    m_userCountersMutex.lock();
    QList<TimeToPicLoggerUserPerformanceCounter *> counters =
            mUserCounters.values();
    m_userCountersMutex.unlock();

    foreach (TimeToPicLoggerUserPerformanceCounter *pC, counters) {
    logValue(QString("UserCounter.%1").arg(pC->counterName()),
             QString::number(pC->counterValue()));
    pC->setCounterValue(0);
  }
}

QString TimeToPicLogger::getTimestamp() {
  // use nanosecond resolution for high accuracy timing
#ifdef USEDATETIME
  QDateTime now = QDateTime::currentDateTime();
  now=now.addMSecs(getTimeStampOffsetMsec());
#define TIMETOPIC_DATETIME_FORMAT                                              \
  "yyyy/MM/dd hh:mm:ss.zzz" // you need to add msecs using Time-function.
  return now.toString(TIMETOPIC_DATETIME_FORMAT);
#else
  return QString::number(m_elapsed.nsecsElapsed() / 1000000000.0, 'f', 6);
#endif
}

LogStreamThread::LogStreamThread(operatingMode operatingMode) {

  mOperatingMode = operatingMode;
  connect(&mTxTimer, &QTimer::timeout, this, &LogStreamThread::txTimeOutSlot);

  if (mOperatingMode == modeClient) {
    connect(&mConnSetupTimer, &QTimer::timeout, this,
            &LogStreamThread::connectionTryTimerTimeTimeoutSlot);

    connect(&mConnSetupTimer, &QTimer::timeout, this,
            &LogStreamThread::connectionTryTimerTimeTimeoutSlot);

    connect(&mUDPDiscoverySocket, SIGNAL(readyRead()), this,
            SLOT(udpSocketReadyReadSlot()));

    mSocket = setupTcpSocket(
        QSharedPointer<QTcpSocket>(nullptr, timeToPicLoggerDoDeleteLater));

    tryConnectionToServer();

  } else if (mOperatingMode == modeServer) {
    mSocketServer = QSharedPointer<QTcpServer>(new QTcpServer,
                                               timeToPicLoggerDoDeleteLater);
    mSocketServer->setMaxPendingConnections(1);

    connect(mSocketServer.data(), &QTcpServer::newConnection, this,
            &LogStreamThread::onNewConnectionRequest);

    mSocketServer->listen(QHostAddress::Any, TIMETOPIC_LOGGING_SERVER_PORT);
  }
}

LogStreamThread::~LogStreamThread() {}

void LogStreamThread::writeRow(const QString &txt) {
#ifdef TTPLOGGING
  if (mSocket.isNull() == false &&
      mSocket->state() == QAbstractSocket::ConnectedState) {
    mBuffer.append(txt);
    if (mTxTimer.isActive() == false) {
      const int txFlushTimerTimeOutMs = 100;
      mTxTimer.start(txFlushTimerTimeOutMs);
    }
  } else {
    /* No connection yet. Drop data */
  }
#else
    Q_UNUSED(txt);
#endif
}

void LogStreamThread::txTimeOutSlot() {
#ifdef TTPLOGGING
  if (mSocket.isNull() == false &&
      mSocket->state() == QAbstractSocket::ConnectedState) {
    for (QString &str : mBuffer) {
      str.append("\r\n");
      mSocket->write(str.toUtf8());
    }
    mBuffer.clear();
    mTxTimer.stop();
  } else if (mSocket.isNull() == false &&
             mSocket->state() == QAbstractSocket::UnconnectedState) {
    mTxTimer.stop();
  }  
#endif
}

QSharedPointer<QTcpSocket>
LogStreamThread::setupTcpSocket(const QSharedPointer<QTcpSocket> &socket) {
  QSharedPointer<QTcpSocket> ret;
  if (socket.isNull() == true) {
    ret = QSharedPointer<QTcpSocket>(new QTcpSocket,
                                     timeToPicLoggerDoDeleteLater);
  } else {
    ret = socket;
  }

  connect(ret.data(), &QTcpSocket::connected, this,
          &LogStreamThread::socketConnectedSlot);

  connect(ret.data(), &QTcpSocket::disconnected, this,
          &LogStreamThread::socketDisconnectedSlot);

  connect(ret.data(), SIGNAL(error(QAbstractSocket::SocketError)), this,
          SLOT(socketErrorSlot(QAbstractSocket::SocketError)));

  return ret;
}

void LogStreamThread::onNewConnectionRequest() {
  auto socket = mSocketServer->nextPendingConnection();
  if (mSocket.isNull() == false) {
    /* reject */
    socket->close();
  } else {
    mSocket = setupTcpSocket(
        QSharedPointer<QTcpSocket>(socket, timeToPicLoggerDoDeleteLater));
    /* Connection ok, install Qt debug handler */
    qInstallMessageHandler(myMessageOutput);
  }
}

void LogStreamThread::connectionTryTimerTimeTimeoutSlot() {
  mConnSetupTimer.stop();
  if (mSocket->state() == QAbstractSocket::UnconnectedState) {
    connectToServer();
  }
}

void LogStreamThread::socketDisconnectedSlot() {
  /* Socket connection is lost */
  /* restore system handler */
  qInstallMessageHandler(nullptr);

  /* Restart connection retry */
  tryConnectionToServer();
}

void LogStreamThread::socketConnectedSlot() {
  /* Connection ok, install Qt debug handler */
  qInstallMessageHandler(myMessageOutput);
}

void LogStreamThread::socketErrorSlot(QAbstractSocket::SocketError /* err */) {}

void LogStreamThread::udpSocketReadyReadSlot() {
  /* We are expecting response packet from logger */
  while (mUDPDiscoverySocket.hasPendingDatagrams()) {
    QByteArray datagram;
    datagram.resize(
        static_cast<int>(mUDPDiscoverySocket.pendingDatagramSize()));
    QHostAddress senderAddr;
    quint16 senderPort;
    mUDPDiscoverySocket.readDatagram(datagram.data(), datagram.size(),
                                     &senderAddr, &senderPort);
    QString myText = QString(datagram);

    /* Process datagram and create connection */
    /* Example messsage "ConnectTo;%1;%2"     */
    const int respPacketSlitCount = 3;
    QStringList mySplits = myText.split(";");
    const QString discoveryResp("ConnectTo");

    if (mySplits.first() == discoveryResp &&
        mySplits.size() == respPacketSlitCount) {
      const QString &destIp = mySplits.at(1);
      quint16 destIpPort = static_cast<quint16>(mySplits.at(2).toUInt());

      if (mSocket->state() == QAbstractSocket::UnconnectedState) {
        connectToServer(destIp, destIpPort);
      }
    }
  }
}

void LogStreamThread::tryConnectionToServer() {
    if (mOperatingMode == modeClient) {
        /* We don't have access to remote.
     * Try connecting to server after small
     * timeOut */
        if (mConnSetupTimer.isActive() == false) {
      const int connectRetryTimerMs = 1000;
      mConnSetupTimer.start(connectRetryTimerMs);
    }
  }
}

void LogStreamThread::connectToServer() {

  if (QString(TIMETOPIC_LOGGING_SERVER_ADDRESS).length() > 0) {

    mSocket->connectToHost(TIMETOPIC_LOGGING_SERVER_ADDRESS,
                           TIMETOPIC_LOGGING_SERVER_PORT, QIODevice::WriteOnly);

    /* Try waiting for connect */
    mSocket->waitForConnected();
  } else {
    /* Try discovery. Needs to match TimeToPic Logger */
    const QString discoveryReq("Hello");
    const int udpDiscoveryPort = 5010;

    QByteArray datagram = discoveryReq.toUtf8();
    mUDPDiscoverySocket.writeDatagram(datagram.data(), datagram.size(),
                                      QHostAddress::Broadcast,
                                      udpDiscoveryPort);
  }
}

void LogStreamThread::connectToServer(const QString &destIp, quint16 destPort) {
  mSocket->connectToHost(destIp, destPort, QIODevice::WriteOnly);

  /* Try waiting for connect */
  mSocket->waitForConnected();
}

TimeToPicLoggerUserPerformanceCounter::TimeToPicLoggerUserPerformanceCounter() {
  mCounterValue = 0;
}

QString TimeToPicLoggerUserPerformanceCounter::counterName() const {
  return mCounterName;
}

void TimeToPicLoggerUserPerformanceCounter::setCounterName(
    const QString &counterName) {
  mCounterName = counterName;
}
quint32 TimeToPicLoggerUserPerformanceCounter::counterValue() const {
  return mCounterValue;
}

void TimeToPicLoggerUserPerformanceCounter::setCounterValue(
    const quint32 &counterValue) {
  QMutexLocker locker(&mCounterMutex);
  mCounterValue = counterValue;
}

void TimeToPicLoggerUserPerformanceCounter::incrementCounter(
    const quint32 &increment) {
  QMutexLocker locker(&mCounterMutex);
  mCounterValue += increment;
}

void reportSender(QObject *sender, QObject *receiver,
                  const QString &messageName) {
  QString senderObjName;
  QString receiverObjName;

  if (sender != nullptr) {
    senderObjName = sender->objectName();
  }
  if (receiver != nullptr) {
    receiverObjName = receiver->objectName();
  }

  if (senderObjName.isEmpty()) {
    senderObjName = QString("obj[%1]").arg(reinterpret_cast<quint64>(sender));
  }
  if (receiverObjName.isEmpty()) {
    receiverObjName =
        QString("obj[%1]").arg(reinterpret_cast<quint64>(receiver));
  }

  TimeToPicLogger::instance()->logMessage(senderObjName, receiverObjName,
                                          messageName);
}

EventScopeObject::EventScopeObject(const QString &channel)
    : m_channel(channel) {
#ifdef TTPLOGGING
  TimeToPicLogger::instance()->logEvent(channel, TimeToPicLogger::EventStart);
#endif
}

EventScopeObject::~EventScopeObject() {
#ifdef TTPLOGGING
  TimeToPicLogger::instance()->logEvent(m_channel, TimeToPicLogger::EventStop);
#endif
}
