# QtInstallers

Application installer creator for Qt applications. Creates installer for Windows and appimage for Linux systems.

# Dependencies 

Windows 

- Windows 10
- NSIS 3 (https://nsis.sourceforge.io/Download), tool added to system path environment (i.e. accessible from cmd)
- 7zip (https://www.7-zip.org/download.html), tool added to system path environment (i.e. accessible from cmd)

Linux 

- Ubuntu 16.0x environment
- Linuxdeployqt (https://github.com/probonopd/linuxdeployqt), tool added to .profile (i.e. accessible from terminal)

# Software to be deployed requirerements
Definition: <root> == root level of software project directory.
Application has deployment directory. Deployment is done there under <root>/Deployment/<platform ("Windows","LinuxUbuntu")> 
Application source directory follow following structure

## Sources:
<root>/src/<stuff>

## Installer gfx 
<root>/src/gfx

## Licence text file 
<root>/licence.txt

## QML stuff:
<root>/src/ui/

## Docs (pdf and txt files): 
<root>/docs 

# Usage 

## Integrate to Qt creator as custom build step 
