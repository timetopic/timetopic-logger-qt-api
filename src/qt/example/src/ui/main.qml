/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.2

ApplicationWindow {
    id: myApp
    visible: true
    width: Screen.pixelDensity*200
    height: Screen.pixelDensity*200
    title: qsTr("Logger Qt/QML example")



    Component.onCompleted: {

        // ### This adds an event into log
        ttplogger.logEvent("QML Window", true)

    }
    Component.onDestruction: {

        // ### This stops an event in current log
        ttplogger.logEvent("QML Window", false)
    }

    property int counter: 0

    Rectangle {
        id: rootBox
        anchors.fill: parent
        anchors.margins: Screen.pixelDensity*1.0

        property real defaultFontSizeMM: 3.0


        Row {
            id: myRow

            property string previousBox

            Column {

                Rectangle {
                    width: rootBox.width/2
                    height: rootBox.height/2
                    property string boxName: "QML Mouse on box1"
                    color:"white"

                    MouseArea {
                        hoverEnabled: true
                        anchors.fill:  parent
                        onEntered: {
                            parent.color="green"
                            ttplogger.logState("QML mouseLocState",parent.boxName)
                            ttplogger.logEvent(parent.boxName,true)

                            ttplogger.logMessage(myRow.previousBox,parent.boxName,"mouse transition")

                        }
                        onExited: {
                            parent.color="white"
                            ttplogger.logEvent(parent.boxName,false)
                            myRow.previousBox = parent.boxName
                        }
                        onMouseXChanged: {
                            ttplogger.logValue(parent.boxName+"mouseX",(mouseX-(x+width/2)).toFixed(0))
                        }
                        onMouseYChanged: {
                            ttplogger.logValue(parent.boxName+"mouseY",(mouseY-(y+height/2)).toFixed(0))
                        }
                    }
                }
                Rectangle {
                    width: rootBox.width/2
                    height: rootBox.height/2
                    property string boxName: "QML Mouse on box3"
                    color:"lightgreen"
                    MouseArea {
                        hoverEnabled: true
                        anchors.fill:  parent
                        onEntered: {
                            parent.color="green"
                            ttplogger.logState("QML mouseLocState",parent.boxName)
                            ttplogger.logEvent(parent.boxName,true)
                            ttplogger.logMessage(myRow.previousBox,parent.boxName,"mouse transition")
                        }
                        onExited: {
                            parent.color="lightgreen"
                            ttplogger.logEvent(parent.boxName,false)
                            myRow.previousBox = parent.boxName
                        }
                    }
                }

            }

            Column {

                Rectangle {
                    width: rootBox.width/2
                    height: rootBox.height/2
                    color:"lightgreen"
                    property string boxName: "QML Mouse on box2"
                    MouseArea {
                        hoverEnabled: true
                        anchors.fill:  parent
                        onEntered: {
                            parent.color="green"
                            ttplogger.logState("QML mouseLocState",parent.boxName)
                            ttplogger.logEvent(parent.boxName,true)
                            ttplogger.logMessage(myRow.previousBox,parent.boxName,"mouse transition")
                        }
                        onExited: {
                            parent.color="lightgreen"
                            ttplogger.logEvent(parent.boxName,false)
                            myRow.previousBox = parent.boxName
                        }
                    }
                }
                Rectangle {
                    width: rootBox.width/2
                    height: rootBox.height/2
                    color:"white"
                    property string boxName: "QML Mouse on box4"
                    MouseArea {
                        hoverEnabled: true
                        anchors.fill:  parent
                        onEntered: {
                            parent.color="green"
                            ttplogger.logState("QML mouseLocState",parent.boxName)
                            ttplogger.logEvent(parent.boxName,true)
                            ttplogger.logMessage(myRow.previousBox,parent.boxName,"mouse transition")
                        }
                        onExited: {
                            parent.color="white"
                            ttplogger.logEvent(parent.boxName,false)
                            myRow.previousBox = parent.boxName
                        }
                    }
                }
            }
        }



        Text {
            anchors.top: rootBox.top
            anchors.left: rootBox.left
            anchors.margins: Screen.pixelDensity*1.0
            text: "Note! Make sure that TimeToPicLogger application is running in localhost!"
            font.pixelSize: Screen.pixelDensity*rootBox.defaultFontSizeMM
        }

        Rectangle {
            anchors.centerIn: parent
            width: parent.width * 0.3
            height: 40
            border.width: 1
            color: "#77aa77"
            Text {
                font.pixelSize: Screen.pixelDensity*rootBox.defaultFontSizeMM
                anchors.centerIn: parent
                text: "Click me!<br> Counter:" + counter
            }


            MouseArea {
                anchors.fill: parent
                onClicked: {
                    // ### This adds a value into log
                    ttplogger.logValue("QML counter", counter);
                    counter = counter + 1;

                    ttplogger.logString("nodes;clear")
                    ttplogger.logString("nodes;create;box1")
                    ttplogger.logString("nodes;create;box2")
                    ttplogger.logString("nodes;create;box3")
                    ttplogger.logString("nodes;create;box4")

                    ttplogger.logString("nodes;addlink;box1;box2")
                    ttplogger.logString("nodes;addlink;box2;box3")
                    ttplogger.logString("nodes;addlink;box3;box4")
                    ttplogger.logString("nodes;addlink;box4;box1")
                    ttplogger.logString("nodes;enableautolayout")
                }
            }
        }
        Column {
            anchors.bottom: rootBox.bottom
            anchors.left: rootBox.left
            anchors.margins: Screen.pixelDensity*1.0
            spacing: Screen.pixelDensity*1

            Row {
            Button {
                text: "Randomize timestamp offset (for testing multisocket timestamp sync)"
                onClicked: {
                    exampleApp.randomizeTimeStampOffset()
                    timeStampOffset.setTimeStampValue(exampleApp.getTimeStampOffset())
                }
            }
            Button {
                text: "Reset"
                onClicked: {
                    exampleApp.randomizeTimeStampOffset()
                    exampleApp.setTimeStampOffset(0)
                    timeStampOffset.setTimeStampValue(exampleApp.getTimeStampOffset())
                }
            }
            }
        }
        Column {
            anchors.bottom: rootBox.bottom
            anchors.right:  rootBox.right
            anchors.margins: Screen.pixelDensity*1.0
            spacing: Screen.pixelDensity*1

            Text {
                id: timeStampOffset

                function setTimeStampValue(value ) {
                    text="Timestamp offset is "+ value.toFixed(2) + " sec"
                }

                font.pixelSize: Screen.pixelDensity*rootBox.defaultFontSizeMM
                text: qsTr("Timestamp offset is 0 sec")

                Component.onCompleted:  {
                    setTimeStampValue(0)
                }
            }
        }
    }
}
