/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
//
#include "exampleqtobject.h"
#include "../timetopiclogger.h"


int main(int argc, char *argv[])
{
    /* Enable high dpi scaling to windows dialogs */
    static const char ENV_VAR_QT_DEVICE_PIXEL_RATIO[] = "QT_DEVICE_PIXEL_RATIO";
    if (!qEnvironmentVariableIsSet(ENV_VAR_QT_DEVICE_PIXEL_RATIO) &&
        !qEnvironmentVariableIsSet("QT_AUTO_SCREEN_SCALE_FACTOR") &&
        !qEnvironmentVariableIsSet("QT_SCALE_FACTOR") &&
        !qEnvironmentVariableIsSet("QT_SCREEN_SCALE_FACTORS")) {
      QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    }

    QApplication app(argc, argv);

/* Set window icon. Some reason icon init should be right after app object creation.
     Otherwise linux version icon does not work. */
#if (defined(_WIN32) || defined(_WIN64))
    QIcon myIcon(":/gfx/applicationIcons/TtpLoggerExample.ico");
#elif (defined(LINUX) || defined(__linux__))
    QIcon myIcon(":/gfx/applicationIcons/TtpLoggerExample.svg");
#endif
    app.setWindowIcon(myIcon);

    QQmlApplicationEngine engine;

    /* Load QML files from resources */
    engine.addImportPath("qrc:///");

    /* Enable OS performance counter monitoring */
    TTPLOG_LOG_OS_COUNTERS;
    TTPLOG_STRING("Example starts");



    /* Example c++ code */
    ExampleQtObject myTestCPlusCplusObj;
    myTestCPlusCplusObj.StartTestTimer();

    engine.rootContext()->setContextProperty("exampleApp", &myTestCPlusCplusObj);
    engine.rootContext()->setContextProperty("ttplogger", TimeToPicLogger::instance());
    engine.load(QUrl(QStringLiteral("qrc:/ui/main.qml")));

    return app.exec();
}
