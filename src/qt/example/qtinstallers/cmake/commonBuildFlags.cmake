if(WIN32)
target_link_libraries(${TARGETNAME}
    PRIVATE   
        psapi
        User32
        Kernel32	
		${COMMON_LIBS}
    )

    target_link_options(${TARGETNAME} PRIVATE -mwindows)

else()
target_link_libraries(${TARGETNAME}
    PRIVATE
        ${COMMON_LIBS}
    )
endif()