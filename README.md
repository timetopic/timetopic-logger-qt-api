# README #

Qt logging API is for application profiling. API macros are written in way that porting to other frameworks&systems should be rather straightforward. Current API should work on Windows and Ubuntu Linux. 
If dropping OS CPU & MEM counters then should work on all Qt supported platforms.

Please keep in mind that TimeToPic bundle can be used logging whatever system that produces files or can send data to socket. Qt Logging API just makes logging convenient since you don't have to implement logger component by yourself. 

### How do I get set up? ###

Clone code somewhere. Open src\qt\example\TtpLoggerExample.pro with Qt Creator. Configure and build. Start TimeToPic Logger, start example you just compiled. 

For integrating your application, you need add files timetopiclogger.cpp and timetopiclogger.h to project. For windows builds, add "win32:LIBS += -lpsapi # for os memory counters" line to pro file. 

Enabling hooks, add "DEFINES+=TTPLOGGING" to compiler options. 

See more from https://timetopic.herwoodtechnologies.com/sample-loggers/qt-logging-api

### Who do I talk to? ###

timetopicprivatesupport@googlegroups.com
