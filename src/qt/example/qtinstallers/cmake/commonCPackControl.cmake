if (${CMAKE_BUILD_TYPE} STREQUAL "Release")
    #https://gitlab.kitware.com/cmake/community/wikis/doc/cpack/PackageGenerators

    # Common CPACK


    set(CPACK_PACKAGE_NAME "${TARGETNAME}")
    set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "${CMAKE_PROJECT_DESCRIPTION}")		
    set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
    set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
    set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})
    set(CPACK_PACKAGE_INSTALL_DIRECTORY "${PROJECT_NAME}")
    set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/license.txt")
    set(CPACK_PACKAGE_EXECUTABLES "${TARGETNAME}" ${TARGETNAME} )

    set(CPACK_PACKAGE_CHECKSUM "MD5" )

    if(WIN32)

        set(PRODUCT_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/src/gfx/applicationIcons/${TARGETNAME}.ico")
        set(CPACK_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/src/gfx/productLogo\\\\productLogoMaster.bmp")

    else()

        set(PRODUCT_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/src/gfx/applicationIcons/${TARGETNAME}.svg")
        set(CPACK_PACKAGE_ICON "${CMAKE_CURRENT_SOURCE_DIR}/src/gfx/productLogo/productLogoMaster.svg")

    endif()


    # NSIS specific

    set(CPACK_NSIS_PACKAGE_NAME "${TARGETNAME}")
    set(CPACK_NSIS_DISPLAY_NAME "${TARGETNAME}")
    set(CPACK_NSIS_ENABLE_UNINSTALL_BEFORE_INSTALL ON)
    set(CPACK_NSIS_MUI_ICON "${PRODUCT_PACKAGE_ICON}")
    SET(CPACK_NSIS_MUI_UNIICON "${PRODUCT_PACKAGE_ICON}")
    SET(CPACK_NSIS_INSTALLED_ICON_NAME "bin\\\\${TARGETNAME}.exe")

    # DEB specifif
    SET(CPACK_DEBIAN_PACKAGE_SECTION "devel")

    # Ensure the Qt runtime libraries are installed
    if(WIN32)

        set(INSTALL_BIN_PATH bin CACHE STRING "Application installation directory")
        set(INSTALL_LIB_PATH bin CACHE STRING "Shared library installation directory")

    else()

        set(INSTALL_BIN_PATH /usr/bin CACHE STRING "Application installation directory")
        set(INSTALL_LIB_PATH /usr/bin CACHE STRING "Shared library installation directory")

    endif()

    # Ensure the output prefix supports multiple build configurations
    if(CMAKE_CONFIGURATION_TYPES)

        set(OUTPUT_PREFIX "${CMAKE_BINARY_DIR}/out/$<CONFIG>")

    else()

        set(OUTPUT_PREFIX "${CMAKE_BINARY_DIR}/out/")

    endif()

    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${OUTPUT_PREFIX}/${INSTALL_BIN_PATH}")
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${OUTPUT_PREFIX}/${INSTALL_LIB_PATH}")

    #Command to install target to destination. Is used by deb and nsis.
    install(TARGETS ${TARGETNAME}
        DESTINATION ${INSTALL_BIN_PATH}
    )

    include(deployHelpers)

    if(WIN32)

        SET(CPACK_GENERATOR "NSIS")
        windeployqt(${TARGETNAME} ${INSTALL_BIN_PATH} ${CMAKE_CURRENT_SOURCE_DIR})

    else()

        SET(CPACK_GENERATOR "DEB")
        SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "${CPACK_PACKAGE_VENDOR}") #required

        #Create appimage and remove --app.. suffix.
        linuxdeployqt(${TARGETNAME} ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})        

        #Add application .desktop and icons to installer
        #We use here /48x48/apps that is also referred on linuxdeployqt function and createAppImage.sh.
        INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.AppDir/usr/share/applications/${TARGETNAME}.desktop DESTINATION share/applications)
        INSTALL(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TARGETNAME}.AppDir/usr/share/icons/hicolor/48x48/apps/${TARGETNAME}.svg DESTINATION share/icons/hicolor/48x48/apps)

    endif()

    include(CPack)

endif()
