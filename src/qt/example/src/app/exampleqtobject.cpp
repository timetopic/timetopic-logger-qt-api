/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include <QDebug>
#include <QtDebug>
#include <QRandomGenerator>
//
#include "../timetopiclogger.h"
#include "exampleqtobject.h"

ExampleQtObject::ExampleQtObject(QObject *parent)
    : QObject(parent), m_myTimerSlotCallCount(0) {
  mTimeSinceStart.start();
  this->setObjectName(__FUNCTION__);
  TTPLOG_POSTCODE("creation of object");
}

void ExampleQtObject::StartTestTimer() {
  TTPLOG_EVENTSCOPE("C++ Timer Test timer active");

  connect(&m_myTimer, SIGNAL(timeout()), this, SLOT(TimerTimeOut()));

  // after 10 seconds there will timeout and interval handler will be triggered.
  const int defaultTimeOutMs = 10000;
  TTPLOG_POSTCODE("timers");
  m_myTimer.start(defaultTimeOutMs);

  TTPLOG_POSTCODE("All set!");
}

void ExampleQtObject::TimerTimeOut() {
  /* Function scope macro that generates entry and exit */
  TTPLOG_FUNCTIONSCOPE;
  TTPLOG_SLOTMESSAGE;

  /* Example of delta value. Move mouse top of channel and use "k" letter show
     graph different way. */
  TTPLOG_VALUE("C++ TimerTimeOutCount", m_myTimerSlotCallCount);
  m_myTimerSlotCallCount++;

  /* Examples of various state machines. Use TimeToPic color palette cycling
   * ('c' letter) for seeing different colors */
  if (m_myTimerSlotCallCount % 2 == 0) {
    TTPLOG_STATE("C++ MyStateMachine", "State A");

    /* Example of delta value. Move mouse top of channel and use TimeToPic math
     * "sum" to see cumulative sum */
    TTPLOG_VALUE(
        QString("InfluxExample|tag1=tagvalue%1|myTag=exampleTagValueA").arg(1),
        1);

  } else if (m_myTimerSlotCallCount % 3 == 0) {
    TTPLOG_STATE("C++ MyStateMachine", "State B");

    TTPLOG_VALUE(
        QString("InfluxExample|tag1=tagvalue%1|myTag=exampleTagValueB").arg(2),
        1);
  } else {
    TTPLOG_STATE("C++ MyStateMachine", "State C");

    TTPLOG_VALUE(
        QString("InfluxExample|tag1=tagvalue%1|myTag=exampleTagValueC").arg(3),
        1);
  }

  if (m_myTimerSlotCallCount % 50 == 0) {
    /* Use logger for finding errors quickly */
    qDebug() << "Example error message. m_myTimerSlotCallCount % 50 == 0";
  }

  qDebug() << "Example debug message C++ timer timeout slot";
  qWarning() << "Example qWarning level message ";
  qCritical() << "Example qCritical";

#if 0
    /* Examples of user counters. */
    TimeToPicLoggerUserPerformanceCounter *c = TTPLOG_GETUSER_COUNTER("testcounter1");
    c->incrementCounter(1);

    TTPLOG_SETUSER_COUNTERVALUE("testcounter2",10);
    TTPLOG_INCREMENTUSER_COUNTERVALUE("testcounter3",1);
#endif

#if 0
    /* If enabled, app will crash intentionally after timeout */
    const int maxRunTimeMs = 60 * 1000;
    if (mTimeSinceStart.elapsed()>maxRunTimeMs) {
        qCritical() << "Now app will crash due of call of qFatal";
        qFatal("Eaxample qFatal for mTimeSinceStart.elapsed()>maxRunTimeMs");
    }
#endif
}

void ExampleQtObject::randomizeTimeStampOffset()
{
    const qreal maxDiffSec = 2.0;
      qreal d = QRandomGenerator::global()->generateDouble();
      qreal offsetSec = maxDiffSec-2*d*maxDiffSec;
      TimeToPicLogger::instance()->setTimeStampOffsetMsec(offsetSec*1000.0);
}

qreal ExampleQtObject::getTimeStampOffset()
{
    return TimeToPicLogger::instance()->getTimeStampOffsetMsec()/1000.0;
}

void ExampleQtObject::setTimeStampOffset(qreal offsetSecs)
{
    TimeToPicLogger::instance()->setTimeStampOffsetMsec(offsetSecs);
}
