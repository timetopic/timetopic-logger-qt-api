/* The MIT License (MIT)

Copyright (c) 2015 Herwood Technologies oy

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#ifndef TIMETOPICLOGGER_H
#define TIMETOPICLOGGER_H
#include <QCoreApplication>
#include <QDebug>
#include <QElapsedTimer>
#include <QHash>
#include <QMutex>
#include <QMutexLocker>
#include <QSharedMemory>
#include <QSharedPointer>
#include <QString>
#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>
#include <QTimer>
#include <QUdpSocket>
//

/* If discovery enabled, this app will try find logger from current IP subnet.
   Current implementation will connect to first responder.
   In normal workstation development (logger and debuggable app runs on same
   machine, discovery is not needed. */
//#define USEDISCOVERY

/* If USEDATETIME is enabled, normal timestamp is added instead elapsed time
 * since start. This is useful when multiple transmitters are sending to same
 * logger from same PC (common clock) */
#define USEDATETIME

/* If SERVERMODE is enabled, TimeToPic logger is expected to connect this
 * instance using port TIMETOPIC_LOGGING_SERVER_PORT using tcp connection. */
//#define SERVERMODE

#ifdef USEDISCOVERY
#define TIMETOPIC_LOGGING_SERVER_ADDRESS ""
#define TIMETOPIC_LOGGING_SERVER_PORT 0
#else
/* If you want specify destination address outside this computer / this subnet,
 * override TIMETOPIC_LOGGING_SERVER_ADDRESS default with your own. */
#define TIMETOPIC_LOGGING_SERVER_ADDRESS "127.0.0.1"
#define TIMETOPIC_LOGGING_SERVER_PORT 5000
#endif

/* Do not edit after this line */

#define BUFFER_MAX_LINE_COUNT 1000

// Define TTPLOGGING to enable these macros
#ifdef TTPLOGGING
#define TTPLOG_EVENTSCOPE(channel) EventScopeObject obj(channel)
// [%1]%2 is TimeToPic Logger Graphviz friendly (scope)
#define TTPLOG_FUNCTIONSCOPE                                                   \
  EventScopeObject funcObj(                                                    \
      QString("[%1]%2")                                                        \
          .arg(reinterpret_cast<quint64>(QThread::currentThreadId()))          \
          .arg(__FUNCTION__))

#define TTPLOG_EVENT(channel, state)                                           \
  TimeToPicLogger::instance()->logEvent(channel, state)

#define TTPLOG_VALUE(channel, value)                                           \
  TimeToPicLogger::instance()->logValue(channel, QString::number(value))

#define TTPLOG_STATE(channel, state)                                           \
  TimeToPicLogger::instance()->logState(channel, state)

#define TTPLOG_STRING(str) TimeToPicLogger::instance()->logString(str)

#define TTPLOG_POSTCODE(str)                                                   \
  TimeToPicLogger::instance()->logString(QString("postcode %1").arg(str))

/* Macro for reporting messaging between entitities */
#define TTPLOG_MESSAGE(source, destination, messagename)                       \
  TimeToPicLogger::instance()->logMessage(source, destination, messagename)

/* Macro for reporting messaging between entitities
 * Call this macro only from QObject slot*/
#define TTPLOG_SLOTMESSAGE reportSender(this->sender(), this, __FUNCTION__);

#ifdef TTPOSCOUNTERS
/* This feature requires win32:LIBS += -lpsapi to be added to application .pro
 * file */
#define TTPLOG_LOG_OS_COUNTERS                                                 \
  TimeToPicLogger::instance()->enableOsPerfCounters(true);
#else
#define TTPLOG_LOG_OS_COUNTERS
#endif
/* User performance counters are reported perioidically and updating them should
 * be rather fast */
// Counters are enabled by calling TTPLOG_LOG_OS_COUNTERS once;

// Slower but easier way to update counter
#define TTPLOG_SETUSER_COUNTERVALUE(counterName, value)                        \
  TimeToPicLogger::instance()->GetCounter(counterName)->setCounterValue(value);

// Slower but easier way to increment counter
#define TTPLOG_INCREMENTUSER_COUNTERVALUE(counterName, value)                  \
  TimeToPicLogger::instance()->GetCounter(counterName)->incrementCounter(value);

// Fast way to access counter
// return TimeToPicLoggerUserPerformanceCounter *. Use it in code for
// incrementing it. If pointer is stored, accessing it via hash is avoided. When
// getting pointer, user needs to take care of conditional compilation when
// TTPLOGGING is not defined.
#define TTPLOG_GETUSER_COUNTER(counterName)                                    \
  TimeToPicLogger::instance()->GetCounter(counterName);

#else
#define TTPLOG_EVENTSCOPE(channel)
#define TTPLOG_FUNCTIONSCOPE
#define TTPLOG_EVENT(channel, state)
#define TTPLOG_VALUE(channel, value)
#define TTPLOG_STATE(channel, state)
#define TTPLOG_STRING(str)
#define TTPLOG_POSTCODE(str)
#define TTPLOG_LOG_OS_COUNTERS
#define TTPLOG_GETUSER_COUNTER(counterName)
#define TTPLOG_SETUSER_COUNTERVALUE(counterName, value)
#define TTPLOG_INCREMENTUSER_COUNTERVALUE(counterName, value)
#define TTPLOG_SLOTMESSAGE
#define TTPLOG_MESSAGE(source, destination, messagename)
#endif

/**
 * @brief This is an example implementation of a logger for Qt applications.
 *
 * Note! This implementation sends log rows to a remote logger: TimeToPicLogger
 * in localhost. Download it from: http://timetopic.herwoodtechnologies.com/
 *
 * This logger can be used from C++ and QML codes.
 *
 * An example of logging a function call:
 * @code{.cpp}
 * void example() {
 *     TTPLOG_EVENTSCOPE("Example function call");
 *     for(int i = 0; i < 5; i++) {
 *         sleep(1);
 *         TTPLOG_VALUE("Example value", i);
 *     }
 * }
 * @endcode
 * You should get log rows like this:
 * @code{.txt}
 * 0.0;event;start;Example function call
 * 1.0;value;0;Example value
 * 2.0;value;1;Example value
 * 3.0;value;2;Example value
 * 4.0;value;3;Example value
 * 5.0;value;4;Example value
 * 5.0001;event;start;Example function call
 * @endcode
 *
 * This logger can also be used from QML when code is initialized like this:
 * @code{.cpp}
 * QQmlApplicationEngine engine;
 * engine.rootContext()->setContextProperty("ttplogger",
 * TimeToPicLogger::instance());
 * engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
 * @endcode
 *
 * Then in QML, you can call functions with Q_INVOKABLE like this:
 * @code{.qml}
 * Item {
 *     Component.onCompleted: {
 *         ttplogger.logEvent("Window", true)
 *     }
 *     Component.onDestruction: {
 *         ttplogger.logEvent("Window", false)
 *     }
 * }
 * @endcode
 */

class TimeToPicLoggerUserPerformanceCounter {

public:
  QString counterName() const;
  TimeToPicLoggerUserPerformanceCounter();
  quint32 counterValue() const;
  void incrementCounter(const quint32 &increment);
  void setCounterName(const QString &counterName);
  void setCounterValue(const quint32 &counterValue);

private:
  QMutex mCounterMutex;
  QString mCounterName;
  quint32 mCounterValue = 0;
};

class TimeToPicLogger : public QThread {
  Q_OBJECT
public:
  /**
   * @brief This function returns pointer to singleton instance of this class.
   *
   * This also starts the log send streaming thread.
   * @return Pointer to this singleton instance.
   */
  static TimeToPicLogger *instance();

  void run();

  void stop();

  /**
   * @brief This enumerator is used when logging Event-type logs with logEvent.
   */
  enum EventState { EventStart, EventStop };

  /**
   * @brief This function logs any free text string.
   * @param text Any string. Does not have to be in TimeToPic format
   */
  Q_INVOKABLE void logString(const QString &text);

  /**
   * @brief This function logs an event start or stop.
   *
   * This creates a log rows like this
   * @code
   * 10.1;event;start;Test channel
   * 10.8;event;stop;Test channel
   * @endcode
   * @param channel Name of the channel
   * @param eventState State of the event.
   */
  Q_INVOKABLE void logEvent(const QString &channel, EventState eventState);

  /**
   * @brief This funciton logs an event start or stop.
   *
   * See logEvent(const QString& channel, EventState eventState) for further
   * details.
   */
  Q_INVOKABLE void logEvent(const QString &channel, bool startEvent);
  Q_INVOKABLE void logMessage(const QString &source, const QString &destination,
                              const QString &messageName);
  Q_INVOKABLE void logState(const QString &channel, const QString &state);
  Q_INVOKABLE void logValue(const QString &channel, const QString &value);

  /**
   * @brief Function for accessing user performance counters
   *
   *
   */
  TimeToPicLoggerUserPerformanceCounter *GetCounter(const QString &counterName);

  Q_INVOKABLE void userPerformanceCounterSet(const QString &counterName,
                                             const quint32 &counterValue);
  Q_INVOKABLE void userPerformanceCounterIncrement(const QString &counterName,
                                                   const quint32 &increment);

  /**
   * @brief Test API function for adjusting timestamp when testing multisocket connections
   */
  Q_INVOKABLE qreal getTimeStampOffsetMsec() const;
  Q_INVOKABLE void setTimeStampOffsetMsec(const qreal &timeStampOffsetMsec);

signals:
  void newLogRow(QString text);

private slots:
  void performanceCounterTimerSlot();

private:
  QElapsedTimer m_elapsed;
  QHash<QString, QString> m_statemachineCurrentState;
  QHash<QString, TimeToPicLoggerUserPerformanceCounter *> mUserCounters;
  QMutex m_mutex;
  QMutex m_userCountersMutex;
  QTimer m_performanceCounterTimer;
  qreal m_prevProcessTime = 0;
  qreal m_prevTotalTime = 0;
  qreal m_timeStampOffsetMsec=0;
  static TimeToPicLogger *m_instance;

  QString getTimestamp();
  TimeToPicLogger();
  void reportUserCounters();
};

class EventScopeObject {
public:
  EventScopeObject(const QString &channel);
  ~EventScopeObject();

private:
  QString m_channel;
};

class LogStreamThread : public QObject {
  Q_OBJECT
public:
  enum operatingMode { modeClient, modeServer };

  LogStreamThread(operatingMode operatingMdoe = modeClient);
  ~LogStreamThread();

public slots:
  void writeRow(const QString &txt);
private slots:

  QSharedPointer<QTcpSocket>
  setupTcpSocket(const QSharedPointer<QTcpSocket> &socket);
  void connectionTryTimerTimeTimeoutSlot();
  void onNewConnectionRequest();
  void socketConnectedSlot();
  void socketDisconnectedSlot();
  void socketErrorSlot(QAbstractSocket::SocketError err);
  void txTimeOutSlot();
  void udpSocketReadyReadSlot();

private:
  QSharedPointer<QTcpServer> mSocketServer;
  QSharedPointer<QTcpSocket> mSocket;
  QStringList mBuffer;
  QTimer mConnSetupTimer;
  QTimer mTxTimer;
  QUdpSocket mUDPDiscoverySocket;
  operatingMode mOperatingMode = modeClient;

  void connectToServer();
  void connectToServer(const QString &destIp, quint16 destPort);
  void tryConnectionToServer();
};

void reportSender(QObject *sender, QObject *receiver,
                  const QString &messageName);

#endif // TIMETOPICLOGGER_H
